# Header sectors

Header length: 17 bytes

## `timestamp`

- Offset: 0
- Length: 8

64 bit unsigned integer

## `chunksQuantity`

- Offset: 8
- Length: 2

16 bit unsigned integer

## `chunkIndex`

- Offset: 10
- Length: 2

16 bit unsigned integer

## `streamId`

- Offset: 12
- Length: 4

32 bit unsigned integer

`0-2` contains `producerId` (24 bit unsigned integer).\
`3` contains `streamIndex` (8 bit unsigned integer) in context of `producerId`.

```
  0x00_00_00_00
    ^^^^^^^^
    Producer ID

  0x00_00_00_00
             ^^
             Stream Index
```

## `packetType`

- Offset: 16
- Length: 1

8 bit unsigned integer

- `0x00` RequestSubscribe
- `0x01` RequestUnsubscribe
- `0x02` DataText
- `0x03` DataFormatJPG
- `0x04` DataFormatMP3
