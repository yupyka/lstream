\documentclass{article}

\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{extramarks}
\usepackage{graphicx,color}
\usepackage{anysize}
\usepackage{amsmath}
\usepackage{natbib}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{float}



% Codeblock
\newcommand{\includecode}[4]{
  \lstinputlisting[float, caption={[#1]#2}, captionpos=b, frame=single, label={#3}]{#4}
}
\lstset{
  basicstyle=\fontsize{8}{12}\selectfont\ttfamily
}

% Figure
\newcommand{\includefigure}[4]{
  \begin{figure}[htb]
  \centering
  \includegraphics{#4}
  \captionsetup{width=.8\linewidth} 
  \caption[#2]{#3}
  \label{#1}
  \end{figure}
}

% Figure (resizable)
\newcommand{\includescalefigure}[5]{
  \begin{figure}[htb]
  \centering
  \includegraphics[width=#4\linewidth]{#5}
  \captionsetup{width=.8\linewidth} 
  \caption[#2]{#3}
  \label{#1}
  \end{figure}
}



% Data
\newcommand{\assignmentTitle}{Assignment 1: Publish/Subscribe Protocol}
\newcommand{\moduleCode}{CSU33031}
\newcommand{\moduleName}{Computer Networks}
\newcommand{\authorName}{Andrii Yupyk}
\newcommand{\authorID}{}
\newcommand{\reportDate}{\printDate}



% Header and footer
\pagestyle{fancy}
\renewcommand\headrulewidth{0.4pt}              % Size of the header
\lhead{\authorName}                             % Top left header
\chead{\moduleCode\ - \assignmentTitle}         % Top center header
\rhead{\firstxmark}                             % Top right header
\renewcommand\footrulewidth{0.4pt}              % Size of the footer
\lfoot{\lastxmark}                              % Bottom left footer
\cfoot{}                                        % Bottom center footer
\rfoot{Page\ \thepage\ of\ \pageref{LastPage}}  % Bottom right footer



% Title
\title{
  \vspace{-1in}
  \begin{figure}[!ht]
  \flushleft
  \includegraphics[width=0.4\linewidth]{reduced-trinity.png}
  \end{figure}
  \vspace{-0.5cm}
  \hrulefill \\
  \vspace{0.5cm}
  \textmd{\textbf{\moduleCode\ \moduleName}}\\
  \textmd{\textbf{\assignmentTitle}}\\
  \vspace{0.125cm}
  \hrulefill \\
}
\author{\textbf{\authorName}}
\date{\today}



% Styles
\textwidth=6.5in
\linespread{1.0}
\setlength\parindent{0pt}
\captionsetup{width=.8\linewidth} 
\lstset{language={}, captionpos=b, frame=single}



% Document
\begin{document}



% ---
% Title
% ---
\maketitle



% ---
% Table of content
% ---
\tableofcontents
\vspace{0.5in}



% ---
% Introduction
% ---
\section{Introduction}
\label{sec:Introduction}

The objective of this assignment is to implement a publish/subscribe protocol over \texttt{UDP} specifically designed for real-time video streaming, but also capable of transferring additional content such as text.

The protocol is based on the concept of a broker, which is a server instance that receives messages from publishers and forwards them to subscribers.
On the other hand we have publishers, which are clients that send messages to the broker, and subscribers, which are clients that receive messages from the broker.

Since this is a protocol designed to work with video streams and additional content such as messages in real-time, it is important to transfer data as efficiently as possible to increase network bandwidth and reduce latency.
Therefore, the protocol is based on \texttt{UDP}, which is a connectionless and unreliable protocol, but it is also very fast and efficient.

Also an equally important aspect of this purpose is the implementation of an efficient header, which should be as small as possible to reduce the size of the message and increase the amount of data that can be transmitted in a single packet, while providing all the necessary metadata to implement the required functionality.



% ---
% Background
% ---
\section{Background}
\label{sec:Background}

I decided to use \texttt{Node.js} runtime for this task, as it is a very popular and powerful platform for network applications with asynchronous programming in mind.
It has very good tooling for asynchronous programming, which is important for this task, as it allows to handle multiple connections simultaneously without blocking the main thread.

For simplicity I use \texttt{TypeScript}, which is a superset of \texttt{JavaScript} that adds static typing to the language. 
This technology helped me a lot in the implementation of the project, since static typing was a very appropriate solution to reduce the mental overhead of the application.

The components of this assignment is implemented as separated \texttt{Node.js} applications, using the \texttt{dgram} module to create a UDP socket and listen for incoming messages as well as send them.

The virtual infrastructure is implemented using \texttt{Docker} and \texttt{Docker Compose}, which allows to run multiple instances of the applications simultaneously and connect them to the same network.



% ---
% Implementation
% ---
\section{Implementation}
\label{sec:Implementation}

The protocol is implemented as a set of \texttt{Node.js} applications, which are separated into three categories: broker, producer and consumer.
The broker is a server application that receives messages from producers and forwards them to consumers.
The producers are client applications that send messages to the broker.
The consumers are client applications that receive messages from the broker sent by the producer.
Below you can see Figure \ref{fig:network-diagram} which shows the relation between these components.

\includescalefigure{fig:network-diagram}{Network diagram}{Network diagram that shows relation between producers, broker and consumers}{0.9}{network.jpg}

As you can see all communication between the components is done through the broker, which is a central point of the network.
The producers and consumers don't know about each other, they only know about the broker and how to communicate with it.

My implementation supports unlimited number of producers and consumers, which can be connected to the broker simultaneously.
The limitation is only the network bandwidth and the hardware resources of the machine that runs the broker.

\includescalefigure{fig:docker-demonstration}{Docker demonstration}{Demonstration of a broker, 3 producers and 5 consumers running simultaneously using Docker and Docker Compose}{0.9}{docker.png}

To run these containers simultaneously you can use the following command:

\begin{verbatim}
    docker-compose up --scale producer=3 --scale consumer=5
\end{verbatim}

% Header
\subsection{Header}
\label{sec:Header}

The header section consists of 17 bytes, divided into 5 fields. 
Table \ref{tab:header-structure} shows the structure of the header.

\begin{table}[!ht]
	\begin{center}
    \begin{tabular}{|l|c|c|c|c|}
      \hline
      \bf Field & \bf Offset & \bf Length & \bf Type \\
      \hline
      timestamp & 0 & 8 & 64-bit unsigned integer \\
      chunksQuantity & 8 & 2 & 16-bit unsigned integer \\
      chunkIndex & 10 & 2 & 16-bit unsigned integer \\
      streamId & 12 & 4 & 32-bit unsigned integer \\
      packetType & 16 & 1 & 8-bit unsigned integer \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Header Structure}
  \label{tab:header-structure}
\end{table}

\begin{itemize} 
  \item The \texttt{timestamp} field is a 64-bit unsigned integer that represents a unix timestamp in milliseconds associated with the packet. Since the protocol is designed to work with real-time video streams, it is important to implement a mechanism to synchronize the video stream between the publisher and the subscriber, so the timestamp field is used for this purpose.
  \item The \texttt{chunksQuantity} field is a 16-bit unsigned integer that represents the number of chunks. It's useful when we have to split the data into chunks and it needs to be reassembled on the subscriber side. 
  \item The \texttt{chunkIndex} field is a 16-bit unsigned integer that represents the index of the current chunk. 
  \item The \texttt{streamId} field is a 32-bit unsigned integer that represents the stream ID. The first 3 bytes of the \texttt{streamId} field contain the \texttt{producerId}, which is a 24-bit unsigned integer. The last byte of the \texttt{streamId} field contains the \texttt{streamIndex}, which is an 8-bit unsigned integer in the context of the \texttt{producerId}. 
  \item The \texttt{packetType} field is an 8-bit unsigned integer that represents the type of the data packet. The possible values for the \texttt{packetType} field are listed in the Table \ref{tab:packet-type-values}.
\end{itemize}

\begin{table}[!ht]
	\begin{center}
    \begin{tabular}{|l|c|}
      \hline
      Value & Description \\
      \hline
      \texttt{0x00} & RequestSubscribe \\
      \texttt{0x01} & RequestUnsubscribe \\
      \texttt{0x02} & DataText \\
      \texttt{0x03} & DataFormatJPG \\
      \texttt{0x04} & DataFormatMP3 \\
      \hline
    \end{tabular}
	\end{center}
  \caption{Packet Type Values}
  \label{tab:packet-type-values}
\end{table}

Listing \ref{lst:header-types}, \ref{lst:header-encode}, \ref{lst:header-decode} shows the implementation of the header decode/encode functions and corresponding types.

\begin{lstlisting}[caption={[Header types]Header types and constants}, label={lst:header-types}]
export type Header = {
  timestamp: number
  chunksQuantity: number
  chunkIndex: number
  streamId: number
  packetType: PacketType
}

export enum PacketType {
  RequestSubscribe    = 0,
  RequestUnsubscribe  = 1,
  DataText            = 2,
  DataFormatJPG       = 3,
  DataFormatMP3       = 4,
}

export const headerSectors: { 
  [Key in keyof Header]: { offset: number, length: number } 
} = {
  timestamp:      { offset: 0,  length: 8 },
  chunksQuantity: { offset: 8,  length: 2 },
  chunkIndex:     { offset: 10, length: 2 },
  streamId:       { offset: 12, length: 4 },
  packetType:     { offset: 16, length: 1 },
}

export const headerLength = 17
\end{lstlisting}

\begin{lstlisting}[caption={[Header encode implementation]Header encode implementation}, label={lst:header-encode}]
export const encodeHeader = (header: Header): Buffer => {
  const buffer = Buffer.alloc(headerLength)

  buffer.writeBigUInt64LE(BigInt(header.timestamp), headerSectors.timestamp.offset)
  buffer.writeUInt16LE(header.chunksQuantity, headerSectors.chunksQuantity.offset)
  buffer.writeUInt16LE(header.chunkIndex, headerSectors.chunkIndex.offset)
  buffer.writeUInt32LE(header.streamId, headerSectors.streamId.offset)
  buffer.writeUInt8(header.packetType, headerSectors.packetType.offset)

  return buffer
}
\end{lstlisting}

\begin{lstlisting}[caption={[Header decode implementation]Header decode implementation}, label={lst:header-decode}]
export const decodeHeader = (buffer: Buffer): Header | null => {
  if (buffer.length < headerLength) return null

  return {
    timestamp: Number(buffer.readBigUInt64LE(headerSectors.timestamp.offset)),
    chunksQuantity: Number(buffer.readUInt16LE(headerSectors.chunksQuantity.offset)),
    chunkIndex: Number(buffer.readUInt16LE(headerSectors.chunkIndex.offset)),
    streamId: Number(buffer.readUInt32LE(headerSectors.streamId.offset)),
    packetType: Number(buffer.readUInt8(headerSectors.packetType.offset)),
  }
}
\end{lstlisting}

Since the protocol works with real-time data, the error correction mechanism is not implemented, as it would increase the latency of the data transfer, which is not acceptable in this case.
It's better to lose a few frames of video than to increase the latency of the stream.

% Broker
\subsection{Broker}
\label{sec:Broker}

The broker is a server application that receives messages from producers and forwards them to consumers.
It is implemented as a \texttt{Node.js} application, which uses the \texttt{dgram} module to create a UDP socket and listen for incoming messages as well as send them. The broker process each incoming message asynchronously, so it doesn't block the main thread and can handle multiple clients simultaneously.

To implement the subscribe/notify functionality the observer pattern is used (see Listing \ref{lst:observable}), where the broker is the subject and the subscribers are the observers.

\begin{lstlisting}[caption={[Observable]Observer pattern implementation}, label={lst:observable}]
export type Subscriber<M> = (message: M) => void

export type Observable<T, M> = {
  subscribe: (topic: T, subscriber: Subscriber<M>) => Subscriber<M>
  unsubscribe: (topic: T, subscriber: Subscriber<M>) => boolean
  notify: (topic: T, message: M) => void
}

export const createObservable = <T, M>(): Observable<T, M> => {
  const subscribers = new Map<T, Set<Subscriber<M>>>()

  return {
    subscribe: (topic, subscriber) => {
      if (!subscribers.has(topic)) subscribers.set(topic, new Set())
      subscribers.get(topic)?.add(subscriber)
      return subscriber
    },

    unsubscribe: (topic, subscriber) => {
      return subscribers.get(topic)?.delete(subscriber) ?? false
    },

    notify: (topic, message) => {
      subscribers.get(topic)?.forEach((subscriber) => subscriber(message))
    },
  }
}
\end{lstlisting}

The broker implements the way to notify the subscribers about the new messages from the publishers. Every time the broker receives a message from the publisher, it notifies all the subscribers that are subscribed to the corresponding stream (topic), calling \texttt{notify} method of the \texttt{Observable} object instance. 

After, every registered callback function that subscribed to that topic is called with the message as an argument (see Listing \ref{lst:notify}, \ref{lst:subscribe}).

\begin{lstlisting}[caption={[Notify functionality]Notify functionality}, label={lst:notify}]
observable.notify(header.streamId, message)
return Promise.resolve(header)
\end{lstlisting}

\begin{lstlisting}[caption={[Subscribe functionality]Subscribe functionality}, label={lst:subscribe}]
const subscriberKey = `${header.streamId}-${host}:${port}`
if (subscribers.has(subscriberKey)) 
  return Promise.reject(`${subscriberKey} is subscribed`)

const subscriber = observable.subscribe(header.streamId, (message) => {
  socket.send(message, port, host)
})

subscribers.set(subscriberKey, subscriber)
return Promise.resolve(header)
\end{lstlisting}

The unsubscribe functionality is implemented in a similar way, but instead of calling \texttt{subscribe} method of the \texttt{Observable} object instance, it calls \texttt{unsubscribe} method (see Listing \ref{lst:unsubscribe}).

\begin{lstlisting}[caption={[Unsubscribe functionality]Unsubscribe functionality}, label={lst:unsubscribe}]
const subscriberKey = `${header.streamId}-${host}:${port}`
const subscriber = subscribers.get(subscriberKey)
if (subscriber === undefined) 
  return Promise.reject(`${subscriberKey} isn't subscribed`)

subscribers.delete(subscriberKey)
observable.unsubscribe(header.streamId, subscriber)
return Promise.resolve(header)
\end{lstlisting}

% Producer
\subsection{Producer}
\label{sec:Producer}

The producer is a client application that sends messages to the broker.
It is implemented as a \texttt{Node.js} application that uses the \texttt{dgram} module to connect to a UDP socket to communicate with the broker.
The producer application has a command line interface that allows to assign a Producer ID to the producer and specify a stream source.

The stream source has to have at least one data source, which is a directory with files of the same format.

\begin{itemize}
  \item The format field stands for the format of the file, which is used to identify the file extension and to specify the \texttt{PacketType} header field.
  \item The prefix field stands for the prefix of the file name, which is used to identify the file.
  \item The path field is the path to the directory with the files.
  \item The size field is the number of files in the directory.
  \item The rate field is the number of messages per second.
\end{itemize}

The producer parses the \texttt{"../sources/metadata.json"} file to get the list of available stream sources and their metadata (see Listing \ref{lst:metadata}).

\begin{lstlisting}[caption={[metadata.json example]An example of metadata.json}, label={lst:metadata}]
{
  "Lagtrain": [
    {
      "format": "mp3",
      "prefix": "fragment",
      "path": "./lagtrain/audio",
      "size": 252,
      "rate": 1
    },
    {
      "format": "jpg",
      "prefix": "frame",
      "path": "./lagtrain/video",
      "size": 3777,
      "rate": 15
    }
  ],

  "Lorem ipsum": [
    {
      "format": "txt",
      "prefix": "paragraph",
      "path": "./lorem-ipsum",
      "size": 10,
      "rate": 0.25
    }
  ]
}
\end{lstlisting}

It is important to send messages with a precise rate, in order to do this I implemented a self-adjusting timer, which calculates a delay between messages based on the rate of a data source and the time it took to send the previous message (see Listing \ref{lst:timer}).

\begin{lstlisting}[caption={[Timer implementation]Timer implementation}, label={lst:timer}]
let i = 0
let interval = 1000 / rate
let expected = Date.now() + interval

const step = async () => {
  // Logic to send a message to the broker
  // (not included in order to reduce the size of the listing)

  if (++i < size) {
    setTimeout(step, Math.max(0, interval - (Date.now() - expected)))
    expected += interval
  }
}
\end{lstlisting}

The producer is able to have multiple streams simultaneously and every stream is going to have a unique Stream Index in context of the Producer ID.

% Consumer
\subsection{Consumer}
\label{sec:Consumer}

The consumer is a client application which is able to subscribe/unsubscribe to/from streams and receive messages from the broker sent by the producer.

It is implemented as a \texttt{Node.js} application that uses the \texttt{dgram} module to connect to a UDP socket to communicate with the broker. 

The consumer application has a command line interface that allows to subscribe to a stream by specifying the stream ID and unsubscribe from a stream by specifying the stream ID, as well as change the preview mode to show the received data in the terminal window.

Below you can see the list of available commands:

\begin{itemize}
  \item \texttt{subscribe <streamId in HEX>} - subscribe to a stream
  \item \texttt{unsubscribe <streamId in HEX>} - unsubscribe from a stream
  \item \texttt{silent <true|false>} - change the preview mode
  \item \texttt{colorful <true|false>} - change the color mode for the preview
\end{itemize}

The consumer can be subscribed to multiple streams simultaneously, but it can't subscribe to the same stream twice.

It saves the received data to the corresponding directory on the consumer side (see Figure \ref{fig:directory-demonstration}).



% ---
% Discussion
% ---
\section{Discussion}
\label{sec:Discussion}

The protocol is designed to work with real-time video streams, so it is important to transfer data as efficiently as possible to increase network bandwidth and reduce latency. 
To achieve this, the header is designed to be as small as possible to reduce the size of the message and increase the amount of data that can be transmitted in a single packet, while providing all the necessary metadata to implement the required functionality. 

However, I had no time to implement in code the functionality to split the data into chunks and reassemble it on the subscriber side, so currently the size limitation of an individual file is \texttt{(64 * 1024 - 1 - 20 - 8 - 17)KB}. 
But on the other side this feature is not required for the protocol to work and it can be easily implemented in the future as header design allows to implement this feature without breaking changes.

Also for real usage of the protocol it is important to implement some form of encryption to protect the data from being intercepted by a third party as well as some form of authentication for consumers and producers to prevent unauthorized access to the broker.

In terms of scalability, I believe that I did a pretty good job, as the protocol is designed to work with an unlimited number of producers and consumers, which can be connected to the broker simultaneously. It is achieved by using the asynchronous programming approach in as many places as possible, so the broker can handle multiple clients simultaneously without blocking the main thread.
For the real-world usage it is possible to implement some form of microservice architecture, where the broker is a cluster of servers, which can be scaled horizontally to handle more clients.

\pagebreak



% ---
% Summary
% ---
\section{Summary}
\label{sec:Summary}

I believe that I have successfully implemented the publish/subscribe protocol with all required functionality and additional features such as the content preview functionality.
Additionally I implemented a Lua dissector for Wireshark, which can be used to analyze the network traffic of the protocol.

Let's look at the demonstration of the protocol in action:

\includescalefigure{fig:usage-demonstration}{Usage demonstration}{Demonstration of running 2 streams simultaneously}{0.8}{demonstration.png}

As you can see on Figure \ref{fig:usage-demonstration}, the producers are running 2 streams simultaneously, one of them is a text stream and the other one is a video/audio stream.

The first consumer subscribes to the text stream "Lorem ipsum" and receives messages from the broker, but then it unsubscribes from the text stream and stops receiving messages from the broker.

The second consumer is subscribed to the video/audio stream "Lagtrain" by Inabakumori and receives messages from the broker. 
It shows frames in the terminal window using the function I wrote to convert the image binary data to an ASCII-art representation of the image. 
This functionality is turned on by default, use the \texttt{silent false} consumer command to turn on the preview mode.

Data received from the broker is saved to the corresponding directory on the consumer side (see Figure \ref{fig:directory-demonstration}).

\includescalefigure{fig:directory-demonstration}{Directory demonstration}{Demonstration of the directory with received data (0x200 == 512)}{0.7}{directory.png}

Also I provided below the demonstration of my lua dissector for Wireshark, which can be used to analyze the network traffic of the protocol (see Figure \ref{fig:wireshark-demonstration}).

\includescalefigure{fig:wireshark-demonstration}{Wireshark demonstration}{Demonstration of the Lua dissector for Wireshark}{0.8}{wireshark.png}



% ---
% Reflection
% ---
\section{Reflection}
\label{sec:Reflection}

In conclusion, I would like to say that this assignment was a very interesting and challenging task for me.
I learned a lot of new things about network protocols and how they work, as well as how to implement them in practice.

I also learned how to use \texttt{Docker} and \texttt{Docker Compose} to run multiple instances of the application simultaneously and connect them to the same network, as well as how to use \texttt{Wireshark} to analyze the network traffic of the protocol and how to write a lua dissector for it.

I used on practice the knowledge I have about \texttt{Node.js}, \texttt{TypeScript} and asynchronous programming, which was very useful for this task.

This is the first time when I implemented a self-adjusting timer, which was a very interesting experience for me, as well as the implementation of the ASCII-art image preview functionality.



% ---
% References
% ---
\bibliographystyle{plain}
\bibliography{References} 
\nocite{*}



\end{document}
