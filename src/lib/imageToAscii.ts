import Jimp from 'jimp'

export enum AsciiArtChars {
  Colorful = '*#%%@@@',
  Grayscale = ' .:-=+*#%@',
}

export type Options = {
  colorful: boolean
  chars: string
  size: number | { width: number; height: number }
  stretch: { width: number; height: number }
}

const defaultOptions: Options = {
  colorful: true,
  chars: AsciiArtChars.Colorful,
  size: 64,
  stretch: { width: 2, height: 1 },
}

export const imageToAscii = async (buffer: Buffer, options: Partial<Options> = defaultOptions) => {
  const _options: Options = { ...defaultOptions, ...options }
  const image = await Jimp.read(buffer)

  if (typeof _options.size === 'object') {
    image.resize(_options.size.width, _options.size.height)
  } else {
    const _width = image.bitmap.width * _options.stretch.width
    const _height = image.bitmap.height * _options.stretch.height
    const widthRatio = _width < _height ? _width / _height : 1
    const heightRatio = _width > _height ? _height / _width : 1
    image.resize(_options.size * widthRatio, _options.size * heightRatio)
  }

  let asciiArt = ''

  image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
    const red = image.bitmap.data[idx]
    const green = image.bitmap.data[idx + 1]
    const blue = image.bitmap.data[idx + 2]

    asciiArt += _options.colorful
      ? getColorfulAsciiChar(red, green, blue, _options.chars)
      : getAsciiChar(red, green, blue, _options.chars)

    if (x === image.bitmap.width - 1) asciiArt += '\n'
  })

  return asciiArt
}

const getColorfulAsciiChar = (red: number, green: number, blue: number, chars: string) => {
  const luminance = Math.floor(0.2126 * red + 0.7152 * green + 0.0722 * blue) / 255
  const i = Math.floor(luminance * (chars.length - 1))
  return `\x1b[1m\x1b[38;2;${red};${green};${blue}m${chars[i]}\x1b[0m`
}

const getAsciiChar = (red: number, green: number, blue: number, chars: string) => {
  const luminance = Math.floor(0.2126 * red + 0.7152 * green + 0.0722 * blue) / 255
  const i = Math.floor(luminance * (chars.length - 1))
  return chars[i]
}
