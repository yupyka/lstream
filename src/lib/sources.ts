import { readFile } from 'fs/promises'
import { PacketType } from './header.js'

export const sourcesUrl = new URL('../../sources/', import.meta.url)

export type SourceMetadata = {
  format: string
  prefix: string
  path: string
  size: number
  rate: number
}

export const formatsPacketTypes: Partial<Record<string, PacketType>> = {
  txt: PacketType.DataText,
  jpg: PacketType.DataFormatJPG,
  mp3: PacketType.DataFormatMP3,
}

export const packetTypesFormats: Partial<Record<PacketType, string>> = {
  [PacketType.DataText]: 'txt',
  [PacketType.DataFormatJPG]: 'jpg',
  [PacketType.DataFormatMP3]: 'mp3',
}

export const getSourcesMetadata = async (
  url = './metadata.json',
  base = sourcesUrl
): Promise<{ [key: string]: SourceMetadata[] }> => {
  return JSON.parse(await readFile(new URL(url, base), { encoding: 'utf-8' }))
}
