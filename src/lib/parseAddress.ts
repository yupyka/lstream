export const parseAddress = (address: string): { host: string; port: number } => {
  const match = address.match(/^([^\:]+)(?:\:(\d+))?$/)

  if (!match) throw new Error('invalid IP address format')

  const host = match[1]
  const port = match[2] ? parseInt(match[2], 10) : 0

  return { host, port }
}
