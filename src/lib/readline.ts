import { createInterface } from 'readline/promises'

const readline = createInterface(process.stdin, process.stdout)
process.on('SIGTERM', () => readline.close())

export const readLine = async (query: string) => {
  return await readline.question(query)
}
