export type Header = {
  timestamp: number
  chunksQuantity: number
  chunkIndex: number
  streamId: number
  packetType: PacketType
}

// prettier-ignore
export enum PacketType {
  RequestSubscribe    = 0,
  RequestUnsubscribe  = 1,
  DataText            = 2,
  DataFormatJPG       = 3,
  DataFormatMP3       = 4,
}

// prettier-ignore
export const headerSectors: { [Key in keyof Header]: { offset: number, length: number } } = {
  timestamp:      { offset: 0,  length: 8 },
  chunksQuantity: { offset: 8,  length: 2 },
  chunkIndex:     { offset: 10, length: 2 },
  streamId:       { offset: 12, length: 4 },
  packetType:     { offset: 16, length: 1 },
}

export const headerLength = 17

/* Payload limit
 * IP packet limit: (64 * 1024 - 1)
 * IP header size: 20
 * UDP header size: 8
 */
export const payloadLimit = 64 * 1024 - 1 - 20 - 8 - headerLength

export const decodeHeader = (buffer: Buffer): Header | null => {
  if (buffer.length < headerLength) return null

  return {
    timestamp: Number(buffer.readBigUInt64LE(headerSectors.timestamp.offset)),
    chunksQuantity: Number(buffer.readUInt16LE(headerSectors.chunksQuantity.offset)),
    chunkIndex: Number(buffer.readUInt16LE(headerSectors.chunkIndex.offset)),
    streamId: Number(buffer.readUInt32LE(headerSectors.streamId.offset)),
    packetType: Number(buffer.readUInt8(headerSectors.packetType.offset)),
  }
}

export const encodeHeader = (header: Header): Buffer => {
  const buffer = Buffer.alloc(headerLength)

  buffer.writeBigUInt64LE(BigInt(header.timestamp), headerSectors.timestamp.offset)
  buffer.writeUInt16LE(header.chunksQuantity, headerSectors.chunksQuantity.offset)
  buffer.writeUInt16LE(header.chunkIndex, headerSectors.chunkIndex.offset)
  buffer.writeUInt32LE(header.streamId, headerSectors.streamId.offset)
  buffer.writeUInt8(header.packetType, headerSectors.packetType.offset)

  return buffer
}

export const encodeStreamId = (producerId: number, streamIndex: number) => {
  const streamId = Buffer.alloc(4)
  streamId.writeUInt8(streamIndex, 0)
  streamId.writeUIntLE(producerId, 1, 3)
  return streamId
}
