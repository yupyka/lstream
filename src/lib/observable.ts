export type Subscriber<M> = (message: M) => void

export type Observable<T, M> = {
  subscribe: (topic: T, subscriber: Subscriber<M>) => Subscriber<M>
  unsubscribe: (topic: T, subscriber: Subscriber<M>) => boolean
  notify: (topic: T, message: M) => void
}

export const createObservable = <T, M>(): Observable<T, M> => {
  const subscribers = new Map<T, Set<Subscriber<M>>>()

  return {
    subscribe: (topic, subscriber) => {
      if (!subscribers.has(topic)) subscribers.set(topic, new Set())
      subscribers.get(topic)?.add(subscriber)
      return subscriber
    },

    unsubscribe: (topic, subscriber) => {
      return subscribers.get(topic)?.delete(subscriber) ?? false
    },

    notify: (topic, message) => {
      subscribers.get(topic)?.forEach((subscriber) => subscriber(message))
    },
  }
}
