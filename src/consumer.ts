import { mkdir, writeFile } from 'fs/promises'
import { type Socket, createSocket } from 'dgram'
import { readLine } from './lib/readline.js'
import { parseAddress } from './lib/parseAddress.js'
import { PacketType, encodeHeader, decodeHeader, headerLength } from './lib/header.js'
import { imageToAscii, Options as AsciiOptions, AsciiArtChars } from './lib/imageToAscii.js'
import { packetTypesFormats } from './lib/sources.js'

const brokerAddress = process.env.BROKER_ADDRESS
if (brokerAddress === undefined) throw new Error('$BROKER_ADDRESS is not set')

const asciiOptionsColorful = { size: 120, colorful: true, chars: AsciiArtChars.Colorful }
const asciiOptionsGrayscale = { size: 120, colorful: false, chars: AsciiArtChars.Grayscale }

type Options = {
  silent: boolean
  asciiOptions: Partial<AsciiOptions>
}

const options: Options = {
  silent: true,
  asciiOptions: asciiOptionsColorful,
}

const startSocket = (address: string) => {
  const { host, port } = parseAddress(address)
  const socket = createSocket('udp4')

  socket.on('listening', () => {
    console.log(`Listening to socket ${host}:${port}`)
    handleListen(socket, host, port)
  })

  socket.on('error', (error) => {
    socket.close()
    throw new Error(error.message)
  })

  socket.on('close', () => {
    console.log(`Socket ${host}:${port} closed`)
  })

  socket.on('message', (message, remote) => {
    handleMessage(message)
  })

  socket.connect(port, host)
  return socket
}

const handleListen = async (socket: Socket, host: string, port: number) => {
  const handlers: { [key: string]: (input: string[]) => void } = {
    subscribe: (input: string[]) => {
      const streamId = Math.abs(parseInt(input[1], 16))

      if (Number.isNaN(streamId) || streamId > 4294967295) {
        console.error(`Invalid Stream ID`)
        return
      }

      const header = encodeHeader({
        timestamp: Date.now(),
        chunksQuantity: 1,
        chunkIndex: 0,
        streamId: streamId,
        packetType: PacketType.RequestSubscribe,
      })

      socket.send(header, port, host)
      console.log(`Subscribed to Stream ID ${streamId.toString(16).toUpperCase()}`)
    },

    unsubscribe: (input: string[]) => {
      const streamId = Math.abs(parseInt(input[1], 16))

      if (Number.isNaN(streamId) || streamId > 4294967295) {
        console.error(`Invalid Stream ID`)
        return
      }

      const header = encodeHeader({
        timestamp: Date.now(),
        chunksQuantity: 1,
        chunkIndex: 0,
        streamId: streamId,
        packetType: PacketType.RequestUnsubscribe,
      })

      socket.send(header, port, host)
      console.log(`Unsubscribed from Stream ID ${streamId.toString(16).toUpperCase()}`)
    },

    silent: (input: string[]) => {
      if (input[1] === 'true') {
        options.silent = true
        console.log(`Enabled silent mode`)
      } else if (input[1] === 'false') {
        options.silent = false
        console.log(`Disabled silent mode`)
      } else {
        console.log(`Invalid argument`)
      }
    },

    colorful: (input: string[]) => {
      if (input[1] === 'true') {
        options.asciiOptions = asciiOptionsColorful
        console.log(`Enabled colorful mode`)
      } else if (input[1] === 'false') {
        options.asciiOptions = asciiOptionsGrayscale
        console.log(`Disabled colorful mode`)
      } else {
        console.log(`Invalid argument`)
      }
    },
  }

  while (true) {
    const input = ((await readLine('> ')) as string).split(' ')
    const command = input[0]

    if (!handlers.hasOwnProperty(command)) {
      console.error(`Invalid command`)
      continue
    }

    handlers[command](input)
  }
}

const handleMessage = async (message: Buffer) => {
  const header = decodeHeader(message)
  const payload = message.subarray(headerLength)

  const filename = `${header?.timestamp}.${packetTypesFormats[header?.packetType ?? 2]}`
  const path = new URL(`../streams/${header?.streamId}/`, import.meta.url)
  const url = new URL(filename, path)

  try {
    await mkdir(path, { recursive: true })
    await writeFile(url, payload)
  } catch (error) {
    console.error(`Unable to write "${url}":`)
    console.error(error)
  }

  if (options.silent === false) {
    if (header?.packetType === PacketType.DataText) {
      console.log(`Message from Stream ID ${header.streamId.toString(16)}:`)
      console.log(payload.toString('utf8'))
    } else if (header?.packetType === PacketType.DataFormatJPG) {
      imageToAscii(payload, options.asciiOptions).then((ascii) =>
        process.stdout.write(ascii + '\n')
      )
    }
  }
}

const socket = startSocket(brokerAddress)
process.on('SIGTERM', () => socket.close())
