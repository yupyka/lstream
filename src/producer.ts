import { readFile } from 'fs/promises'
import { type Socket, createSocket } from 'dgram'
import { readLine } from './lib/readline.js'
import { parseAddress } from './lib/parseAddress.js'
import { encodeHeader, encodeStreamId } from './lib/header.js'
import {
  type SourceMetadata,
  getSourcesMetadata,
  sourcesUrl,
  formatsPacketTypes,
} from './lib/sources.js'

const brokerAddress = process.env.BROKER_ADDRESS
if (brokerAddress === undefined) throw new Error('$BROKER_ADDRESS is not set')

const producerId = Math.abs(parseInt((await readLine('> Enter Producer ID: ')) as string, 16))
if (Number.isNaN(producerId) || producerId > 16777215) throw new Error('Invalid Producer ID')

const startSocket = (address: string) => {
  const { host, port } = parseAddress(address)
  const socket = createSocket('udp4')

  socket.on('listening', () => {
    console.log(`Listening to socket ${host}:${port}`)
    handleListen(socket, host, port)
  })

  socket.on('error', (error) => {
    socket.close()
    throw new Error(error.message)
  })

  socket.on('close', () => {
    console.log(`Socket ${host}:${port} closed`)
  })

  socket.connect(port, host)
  return socket
}

const createIndexIssuer = (availableIndex = 0) => {
  return () => availableIndex++
}

const handleListen = async (socket: Socket, host: string, port: number) => {
  const sourcesMetadata = await getSourcesMetadata()
  const getStreamIndex = createIndexIssuer()
  const sourcesAvaileable = `Available sources: [${Object.keys(sourcesMetadata).join(', ')}]`
  const sourcesQuery = `> Choose a source to start the stream: `

  while (true) {
    console.log(sourcesAvaileable)
    const sourceKey = (await readLine(sourcesQuery)) as string

    if (!sourcesMetadata.hasOwnProperty(sourceKey)) {
      console.log(`Invalid source ${sourceKey}`)
      continue
    }

    const sourceMetadata = sourcesMetadata[sourceKey]
    const streamId = encodeStreamId(producerId, getStreamIndex()).readUInt32LE()

    sourceMetadata.forEach((format) => runStream(streamId, format, socket, host, port))
    console.log(`Streaming ${sourceKey} as Stream ID ${streamId.toString(16).toUpperCase()}`)
  }
}

const runStream = async (
  streamId: number,
  { prefix, format, path, size, rate }: SourceMetadata,
  socket: Socket,
  host: string,
  port: number
) => {
  const packetType = formatsPacketTypes[format]

  if (packetType === undefined) {
    console.error(`Unsupported format: ${format}`)
    return
  }

  let i = 0

  let interval = 1000 / rate
  let expected = Date.now() + interval

  const step = async () => {
    const filename = `${prefix}${i}.${format}`
    const url = new URL(`./${path}/${filename}`, sourcesUrl)

    try {
      const payload = await readFile(url)
      const header = encodeHeader({
        timestamp: Date.now(),
        chunksQuantity: 1,
        chunkIndex: 0,
        streamId: streamId,
        packetType: packetType,
      })

      socket.send(Buffer.concat([header, payload]), port, host)
    } catch (error) {
      console.error(`Unable to send "${url}":`)
      console.error(error)
    }

    if (++i < size) {
      setTimeout(step, Math.max(0, interval - (Date.now() - expected)))
      expected += interval
    }
  }

  setTimeout(step, interval)

  return
}

const socket = startSocket(brokerAddress)
process.on('SIGTERM', () => socket.close())
