import { createSocket, Socket } from 'dgram'
import { createObservable, type Subscriber } from './lib/observable.js'
import { parseAddress } from './lib/parseAddress.js'
import { decodeHeader, PacketType } from './lib/header.js'

const brokerAddress = process.env.BROKER_ADDRESS
if (brokerAddress === undefined) throw new Error('$BROKER_ADDRESS is not set')

const observable = createObservable<number, Buffer>()
const subscribers = new Map<string, Subscriber<Buffer>>()

const startSocket = (address: string) => {
  const { host, port } = parseAddress(address)
  const socket = createSocket('udp4')

  socket.on('listening', () => {
    console.log(`Listening to socket ${host}:${port}`)
  })

  socket.on('error', (error) => {
    socket.close()
    throw new Error(error.message)
  })

  socket.on('close', () => {
    console.log(`Socket ${host}:${port} closed`)
  })

  socket.on('message', (buffer, remote) => {
    const { address: host, port } = remote

    handleMessage(socket, host, port, buffer)
      .then((header) => {
        console.log(`Handle message from ${remote.address}:${remote.port}:`, JSON.stringify(header))
      })
      .catch((reason) => {
        console.error(`Unable to handle message from ${remote.address}:${remote.port}:`, reason)
      })
  })

  socket.bind(port, host)
  return socket
}

const handleMessage = async (socket: Socket, host: string, port: number, message: Buffer) => {
  const header = decodeHeader(message)
  if (header === null) return Promise.reject('invalid header')

  const subscribe = () => {
    const subscriberKey = `${header.streamId}-${host}:${port}`
    if (subscribers.has(subscriberKey)) return Promise.reject(`${subscriberKey} is subscribed`)

    const subscriber = observable.subscribe(header.streamId, (message) => {
      socket.send(message, port, host)
    })

    subscribers.set(subscriberKey, subscriber)
    return Promise.resolve(header)
  }

  const unsubscribe = () => {
    const subscriberKey = `${header.streamId}-${host}:${port}`
    const subscriber = subscribers.get(subscriberKey)
    if (subscriber === undefined) return Promise.reject(`${subscriberKey} isn't subscribed`)

    subscribers.delete(subscriberKey)
    observable.unsubscribe(header.streamId, subscriber)
    return Promise.resolve(header)
  }

  const broadcast = () => {
    observable.notify(header.streamId, message)
    return Promise.resolve(header)
  }

  const packetTypeHandlers = {
    [PacketType.RequestSubscribe]: subscribe,
    [PacketType.RequestUnsubscribe]: unsubscribe,
    [PacketType.DataText]: broadcast,
    [PacketType.DataFormatJPG]: broadcast,
    [PacketType.DataFormatMP3]: broadcast,
  }

  return packetTypeHandlers[header.packetType]?.() ?? Promise.reject('unknown packet type')
}

const socket = startSocket(brokerAddress)
process.on('SIGTERM', () => socket.close())
