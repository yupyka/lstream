lstream_protocol = Proto("LStream", "LStream Protocol")

packet_types = {
  [0]="RequestSubscribe",
  [1]="RequestUnsubscribe",
  [2]="DataText",
  [3]="DataFormatJPG",
  [4]="DataFormatMP3",
}

timestamp       = ProtoField.uint64("lstream.timestamp",       "Timestamp",       base.DEC)
chunks_quantity = ProtoField.uint16("lstream.chunks_quantity", "Chunks Quantity", base.DEC)
chunk_index     = ProtoField.uint16("lstream.chunk_index",     "Chunk Index",     base.DEC)
stream_id       = ProtoField.uint32("lstream.stream_id",       "Stream ID",       base.HEX)
packet_type     = ProtoField.uint8 ("lstream.packet_type",     "Packet Type",     base.DEC)

lstream_protocol.fields = { timestamp, chunks_quantity, chunk_index, stream_id, packet_type }

function formatTimestamp(timestamp)
  return os.date("%Y-%m-%d %H:%M:%S", math.floor(timestamp / 1000)) .. string.format(".%03d", timestamp % 1000)
end

function lstream_protocol.dissector(buffer, pinfo, tree)
  length = buffer:len()
  if length < 17 then return end

  pinfo.cols.protocol = lstream_protocol.name

  local subtree = tree:add(lstream_protocol, buffer(), "LStream Protocol Data")

  subtree:add_le(timestamp,       buffer(0, 8)):append_text(" (" .. formatTimestamp(buffer(0, 8):le_uint64():tonumber()) .. ")")
  subtree:add_le(chunks_quantity, buffer(8, 2))
  subtree:add_le(chunk_index,     buffer(10, 2))
  subtree:add_le(stream_id,       buffer(12, 4))
  subtree:add_le(packet_type,     buffer(16, 1)):append_text(" (" .. packet_types[buffer(16, 1):le_uint64(0, 1):tonumber()] .. ")")
end

local tcp_port = DissectorTable.get("udp.port")
tcp_port:add(5000, lstream_protocol)
